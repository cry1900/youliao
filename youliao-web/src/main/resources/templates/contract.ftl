<para align="center" lineSpacing="2" fillColor="#ff99ff">
    <text fontStyle="bold" fontSize="20">XXXXXX 集 团 有 限 公 司</text>
</para>
<para lineSpacing="1">
    <rect color="" name="x5246" width="700">
        <para align="center" lineSpacing="2" fillColor="#ff99ff" name="x5246" width="700">
            <text  fontSize="20">       </text>
            <text  fontSize="20">       调拨单</text>
            <text  fontSize="20">       NO.</text>
            <text  fontSize="20">${model.waybillNum!''}</text>
            <text  fontSize="15">                   【${model.versionNum!''}】</text>
        </para>

    </rect>
</para>
<para lineSpacing="1">
    <rect color="" name="x5247" width="350">
        <para align="left" lineSpacing="3" fillColor="#ff99ff" name="x5247" width="350">
            <text  fontSize="14">购货单位:</text>
            <text  fontSize="14">${model.customerName!''}</text>
        </para>
    </rect>
    <rect  color="" name="x52407" width="120">
        <para align="left" lineSpacing="3" fillColor="#ff99ff" name="x52407" width="120">
            <text  fontSize="14">收货人:</text>
            <text  fontSize="14">${model.consigneeName!''}</text>
        </para>
    </rect>
    <rect  color="" name="x52408" width="200">
        <para align="left" lineSpacing="3" fillColor="#ff99ff" name="x52408" width="200">
            <text  fontSize="14">收货人电话:</text>
            <text  fontSize="14">${model.consigneeContact!''}</text>
        </para>
    </rect>
</para>
<para lineSpacing="1">
    <rect color="" name="x5249" width="600">
        <para align="left" lineSpacing="3" fillColor="#ff99ff" name="x5249" width="600">
            <text  fontSize="14">收货地址:</text>
            <text  fontSize="14">${model.consigneeAddress!''}</text>
        </para>
    </rect>
</para>
<para lineSpacing="1">
    <rect color="" name="x6870" width="300">
        <para align="left" lineSpacing="3" fillColor="#ff99ff" name="x6870" width="300">
            <text  fontSize="14">承运单位:</text>
            <text  fontSize="14">XXX物流</text>
        </para>
    </rect>
    <rect  color="" name="x6871" width="150">
        <para align="left" lineSpacing="3" fillColor="#ff99ff" name="x6871" width="150">
            <text  fontSize="14">承运车号:</text>
            <text  fontSize="14">${model.vehicleNo!''}</text>
        </para>
    </rect>
    <rect  color="" name="x6872" width="200">
        <para align="center" lineSpacing="3" fillColor="#ff99ff" name="x6872" width="200">
            <text  fontSize="14">承运人:</text>
            <text  fontSize="14">${model.driverName!''}</text>
        </para>
    </rect>
</para>
<para lineSpacing="1">
    <rect color="" name="x6870" width="300">
        <para align="left" lineSpacing="3" fillColor="#ff99ff" name="x6870" width="300">
            <text  fontSize="14">运输方式:</text>
            <text  fontSize="14">${model.transportType!''}</text>
        </para>
    </rect>
    <rect  color="" name="x6871" width="100">
        <para align="left" lineSpacing="3" fillColor="#ff99ff" name="x6871" width="100">
            <text  fontSize="14">提货方式:</text>
            <text  fontSize="14">${model.pickType!''}</text>
        </para>
    </rect>
    <rect  color="" name="x6872" width="280">
        <para align="right" lineSpacing="3" fillColor="#ff99ff" name="x6872" width="280">
            <text  fontSize="14">办单时间:</text>
            <text  fontSize="14">${model.deliveryTime!''}</text>
        </para>
    </rect>
</para>
<para lineSpacing="1">
    <table cellpadding="0" cellspacing="0" height="400" width="700" sizeType="autoheight" name="x342" header="1"
           cols="75,75,85,75,75,75,75,70,85" rows="" >
        <rect col="1" align="center" row="1" colSpan="4" rowSpan="1" name="x345">
            <para align="center" >
                <text fontSize="12">品名</text>
            </para>
        </rect>
        <rect col="5" align="center" row="1" colSpan="2" rowSpan="1"  name="x345">
            <para align="center">
                <text fontSize="12">规格</text>
            </para>
        </rect>
        <rect col="7" align="center" row="1" colSpan="1" rowSpan="1" name="x345">
            <para align="center">
                <text fontSize="12">单位</text>
            </para>
        </rect>
        <rect col="8" align="center" row="1" colSpan="1" rowSpan="1" name="x345">
            <para align="center">
                <text fontSize="12">数量</text>
            </para>
        </rect>
        <rect col="9" align="center" row="1" colSpan="1" rowSpan="1" name="x345">
            <para align="center">
                <text fontSize="12">产品特性</text>
            </para>
        </rect>

    <#list detailsList as details>

        <rect col="1" align="center" row="${(details_index)+2}" colSpan="4" rowSpan="1" name="x345">
            <para align="center">
                <text fontSize="12">${details.skuName!''}</text>
            </para>
        </rect>
        <rect col="5" align="center" row="${(details_index)+2}" colSpan="2" rowSpan="1" name="x345">
            <para align="center">
                <text fontSize="12">${details.specName!''}</text>
            </para>
        </rect>
        <rect col="7" align="center" row="${(details_index)+2}" colSpan="1" rowSpan="1" name="x345">
            <para align="center">
                <text fontSize="12">${details.unitName!''}</text>
            </para>
        </rect>
        <rect col="8" align="center" row="${(details_index)+2}" colSpan="1" rowSpan="1" name="x345">
            <para align="center">
                <text fontSize="12">${details.weightQuantity!''}</text>
            </para>
        </rect>
        <rect col="9" align="center" row="${(details_index)+2}" colSpan="1" rowSpan="1" name="x345">
            <para align="center">
                <text fontSize="12">${details.remark!''}</text>
            </para>
        </rect>

    </#list>
        <rect col="1" align="center" row="${(count)+2}" colSpan="4" rowSpan="1" name="x345">
            <para align="center">
                <text fontSize="12">合计（小写）</text>
            </para>
        </rect>

        <rect col="5" align="center" row="${(count)+2}" colSpan="5" rowSpan="1" name="x345">
            <para align="center">
                <text fontSize="12">T=${model.sumNum!''}</text>
            </para>
        </rect>

    </table>

</para>

<para lineSpacing="1">
    <rect color="" name="x6870" width="250">
        <para align="left" lineSpacing="3" fillColor="#ff99ff" name="x6870" width="250">
            <text  fontSize="14">发货单位盖章:</text>
            <text  fontSize="14">       </text>
        </para>
    </rect>
    <rect  color="" name="x6871" width="200">
        <para align="left" lineSpacing="3" fillColor="#ff99ff" name="x6871" width="200">
            <text  fontSize="14">收货人签字:</text>
            <text  fontSize="14">        </text>
        </para>
    </rect>
    <rect  color="" name="x6872" width="200">
        <para align="left" lineSpacing="3" fillColor="#ff99ff" name="x6872" width="200">
            <text  fontSize="14">年  月   日</text>
            <text  fontSize="14">      </text>
        </para>
    </rect>
</para>


<para lineSpacing="0">
    <rect color="" name="x6870" width="700">
        <para align="left" lineSpacing="0" fillColor="#ff99ff" name="x6870" width="600">
            <text  fontSize="14">说明：根据国家相关运输规定，经充分协商，订立如下条款，以便三方共同遵守:</text>
        </para>
    </rect>
</para>
<para lineSpacing="0">
    <rect color="" name="x6870" width="700">
        <para align="left" lineSpacing="0" fillColor="#ff99ff" name="x6870" width="600">
            <text  fontSize="14">1、上述货物，承运过程中如有短缺、雨淋、损坏等货物损失，均由承运人负责赔偿。</text>
        </para>
    </rect>
</para>
<para lineSpacing="0">
    <rect color="" name="x6870" width="700">
        <para align="left" lineSpacing="0" fillColor="#ff99ff" name="x6870" width="600">
            <text  fontSize="14">2、实行车箱板交货原则，货物装车后，货物安全责任全部转由承运人负责。</text>
        </para>
    </rect>
</para>
<para lineSpacing="0" sizeType="autoheight">
    <rect color="" name="x6870" width="700" sizeType="autoheight">
        <para align="left" lineSpacing="0"  sizeType="autoheight" fillColor="#ff99ff" name="x6870" width="600">
            <text  fontSize="14">3、承运单位必须保证其提供的运输车辆信息真实、准确，如出现货物损失、被骗、被盗等情况时，概由承运单位、承运人负责全额赔偿。</text>
        </para>
    </rect>
</para>

<para lineSpacing="0">
    <rect color="" name="x6870" width="700">
        <para align="left" lineSpacing="0" fillColor="#ff99ff" name="x6870" width="600">
            <text  fontSize="14">4、如运输车辆、司机下落不明，货物灭失，则承运车主和调派单位全额赔偿。</text>
        </para>
    </rect>
</para>


<para lineSpacing="0">
    <rect color="" name="x6870" width="200">
        <para align="left" lineSpacing="3" fillColor="#ff99ff" name="x6870" width="250">
            <text  fontSize="14">委托单位:XXXX集团</text>
        </para>
    </rect>
    <rect  color="" name="x6871" width="250">
        <para align="left" lineSpacing="3" fillColor="#ff99ff" name="x6871" width="200">
            <text  fontSize="14">承运单位:</text>
            <text  fontSize="14">XXXX物流有限公司</text>
        </para>
    </rect>
    <rect  color="" name="x6872" width="90" >
        <para align="left" lineSpacing="3" fillColor="#ff99ff" name="x6872">
            <text  fontSize="14">承运车主:</text>
        </para>
    </rect>
    <rect align="right" color="" name="x6872" width="60" height="40">
        <img src="${model.signImages}"  height="40" width="50"/>
    </rect>
</para>

<para lineSpacing="0">
    <rect color="" name="x6870" width="200">
        <para align="left" lineSpacing="3" fillColor="#ff99ff" name="x6870" width="250">
            <text  fontSize="14">经办人:办单终端 </text>
        </para>
    </rect>
    <rect  color="" name="x6871" width="250">
        <para align="left" lineSpacing="3" fillColor="#ff99ff" name="x6871" width="200">
            <text  fontSize="14">经办人:</text>
            <text  fontSize="14">${model.quotOrganName!''}</text>
        </para>
    </rect>
    <rect  color="" name="x6872" width="200">
        <para align="left" lineSpacing="3" fillColor="#ff99ff" name="x6872" width="200">
            <text  fontSize="14"></text>
            <text  fontSize="14">      </text>
        </para>
    </rect>
</para>
<para lineSpacing="0">
    <rect color="" name="x6870" width="450" height="60">
        <para align="left" lineSpacing="3" fillColor="#ff99ff" name="x6870" width="450">
            <text  fontSize="14">电话:</text>
            <text  fontSize="14">${model.deliveryContact!''}</text>
        </para>
    </rect>

    <rect  color="" name="x6872" width="60" height="50">
        <para align="left" lineSpacing="3" fillColor="#ff99ff" name="x6872" >
            <text  fontSize="14">指纹:</text>
        </para>
    </rect>
    <rect align="left" color="" name="x6872" width="60" height="60">
        <img src="${model.fingerPrintImages!''}"  height="50" width="60"/>
    </rect>
</para>
<para lineSpacing="0">
    <rect color="" name="x6870" width="700">
        <para align="center" lineSpacing="0" fillColor="#ff99ff" name="x6870" width="700">
            <text  fontSize="12">- - - - - - - - - - - - - - - - - - - - - - - - 门     卫     留     存 - - - - - - - - - - - - - - - - - - - - - - - -</text>
        </para>
    </rect>
</para>
<para lineSpacing="0">
    <rect color="" name="x6870" width="220">
        <para align="left" lineSpacing="0" fillColor="#ff99ff" name="x6870" width="220">
            <text  fontSize="12">车辆预约信息:</text>
            <text  fontSize="12">${model.oppointment!''}</text>
        </para>
    </rect>
    <rect  color="" name="x6872" width="100">
        <para align="center" lineSpacing="0" fillColor="#ff99ff" name="x6872" width="100">
            <text  fontSize="12">运输方式:</text>
            <text  fontSize="12">${model.transportType!''}</text>
        </para>
    </rect>
    <rect  color="" name="x6872" width="300">
        <para align="left" lineSpacing="0" fillColor="#ff99ff" name="x6872" width="300">
            <text  fontSize="12">收货单位:</text>
            <text  fontSize="12"> ${model.customerName!''}</text>

        </para>
    </rect>
    <rect  color="" name="x6872" width="60">
        <para align="left" lineSpacing="0" fillColor="#ff99ff" name="x6872" width="60">
            <text  fontSize="12">  【${model.versionNum!''}】</text>
        </para>
    </rect>
    <rect  color="" name="x6872" width="200">
        <para align="left" lineSpacing="0" fillColor="#ff99ff" name="x6872" width="200">
            <text  fontSize="12">配载单号:</text>
            <text  fontSize="12">${model.stowageNum!''}</text>
        </para>
    </rect>
    <rect  color="" name="x6872" width="150">
        <para align="left" lineSpacing="0" fillColor="#ff99ff" name="x6872" width="150">
            <text  fontSize="12">承运车号:</text>
            <text  fontSize="12">${model.vehicleNo!''}</text>
        </para>
    </rect>
    <rect  color="" name="x6872" width="200">
        <para align="left" lineSpacing="0" fillColor="#ff99ff" name="x6872" width="200">
            <text  fontSize="12">办单时间:</text>
            <text  fontSize="12">${model.deliveryTime!''}</text>
        </para>
    </rect>
    <rect  color="" name="x6872" width="80">
        <para align="left" lineSpacing="0" fillColor="#ff99ff" name="x6872" width="80">
            <text  fontSize="12">发货人签字:</text>
            <text  fontSize="12"></text>
        </para>
    </rect>
</para>
<para lineSpacing="0">
    <table cellpadding="50" cellspacing="0" height="400" width="700" sizeType="autoheight" name="x347" header="1"
           cols="75,75,85,75,75,75,75,70,85" rows="" >

        <rect col="1" align="center" row="1" colSpan="2" rowSpan="6" name="x347">
            <para lineSpacing="0">
                <rect align="left"  color="" name="x5246" width="140" height="140">
                    <img src="${model.codeUrl!''}"  height="140" width="140"/>
                </rect>
            </para>

        </rect>
        <rect col="3" align="center" row="1" colSpan="5" rowSpan="1"  name="x347">
            <para align="center">
                <text fontSize="12">物料名称</text>
            </para>
        </rect>
        <rect col="8" align="center" row="1" colSpan="2" rowSpan="1"  name="x347">
            <para align="center">
                <text fontSize="12">吨位</text>
            </para>
        </rect>
    <#list model.mvDetails as details>
        <rect col="3" align="center" row="${(details_index)+2}" colSpan="5" rowSpan="1" name="x347">
            <para align="center">
                <text fontSize="12">${details.skuName!''}</text>
            </para>
        </rect>
        <rect col="8" align="center" row="${(details_index)+2}" colSpan="2" rowSpan="1" name="x347">
            <para align="center">
                <text fontSize="12">${details.weightQuantity!''}</text>
            </para>
        </rect>
        <rect col="1" align="center" row="7" colSpan="2" rowSpan="1" name="x345">
            <para align="center">
                <text fontSize="12">合计（小写）</text>
            </para>
        </rect>

        <rect col="3" align="center" row="7" colSpan="7" rowSpan="1" name="x345">
            <para align="center">
                <text fontSize="12">T=${model.sumNum!''}</text>
            </para>
        </rect>
    </#list>
    </table>
</para>
<para lineSpacing="8">
    <rect color="" name="x6870" width="700">
        <para align="center" lineSpacing="0" fillColor="#ff99ff" name="x6870" width="700">
            <text  fontSize="12">- - - - - - - - - - - - - - - - - - - - - - - - 化     管     留     存 - - - - - - - - - - - - - - - - - - - - - - - -</text>
        </para>
    </rect>
</para>
<para lineSpacing="0">
    <rect  color="" name="x6872" width="200">
        <para align="center" lineSpacing="0" fillColor="#ff99ff" name="x6872" width="250">
            <text  fontSize="12">配载单号:</text>
            <text  fontSize="12">${model.stowageNum!''}</text>
        </para>
    </rect>

    <rect  color="" name="x6872" width="100">
        <para align="center" lineSpacing="0" fillColor="#ff99ff" name="x6872" width="200">
            <text  fontSize="12">运输方式:</text>
            <text  fontSize="12">${model.transportType!''}</text>
        </para>
    </rect>
    <rect  color="" name="x6872" width="150">
        <para align="center" lineSpacing="0" fillColor="#ff99ff" name="x6872" width="200">
            <text  fontSize="12">承运车号:</text>
            <text  fontSize="12">${model.vehicleNo!''}</text>
        </para>
    </rect>
    <rect  color="" name="x6872" width="230">
        <para align="center" lineSpacing="0" fillColor="#ff99ff" name="x6872" width="230">
            <text  fontSize="12">办单时间:</text>
            <text  fontSize="12">${model.deliveryTime!''}</text>
            <text  fontSize="12">    【${model.versionNum!''}】</text>

        </para>
    </rect>
    <rect  color="" name="x6872" width="300">
        <para align="left" lineSpacing="0" fillColor="#ff99ff" name="x6872" width="300">
            <text  fontSize="12">   收货单位:</text>
            <text  fontSize="12"> ${model.customerName!''}</text>

        </para>
    </rect>

</para>
<para lineSpacing="1">
    <table cellpadding="0" cellspacing="0" height="400" width="700" sizeType="autoheight" name="x342" header="1"
           cols="75,75,85,75,75,75,75,70,85" rows="" >
        <rect col="1" align="center" row="1" colSpan="2" rowSpan="1" name="x345">
            <para align="center" >
                <text fontSize="12">ERP单号</text>
            </para>
        </rect>
        <rect col="3" align="center" row="1" colSpan="3" rowSpan="1"  name="x345">
            <para align="center">
                <text fontSize="12">品名</text>
            </para>
        </rect>
        <rect col="6" align="center" row="1" colSpan="1" rowSpan="1" name="x345">
            <para align="center">
                <text fontSize="12">规格</text>
            </para>
        </rect>
        <rect col="7" align="center" row="1" colSpan="2" rowSpan="1" name="x345">
            <para align="center">
                <text fontSize="12">产品特性</text>
            </para>
        </rect>
        <rect col="9" align="center" row="1" colSpan="2" rowSpan="1" name="x345">
            <para align="center">
                <text fontSize="12">数量</text>
            </para>
        </rect>
        <rect col="11" align="center" row="1" colSpan="1" rowSpan="1" name="x345">
            <para align="center">
                <text fontSize="12">备注</text>
            </para>
        </rect>

    <#list detailsList as details>
        <rect col="1" align="center" row="${(details_index)+2}" colSpan="2" rowSpan="1" name="x345">
            <para align="center">
                <text  fontStyle="bold" fontSize="16">${details.erpNum!''}</text>
            </para>
        </rect>
        <rect col="3" align="center" row="${(details_index)+2}" colSpan="3" rowSpan="1" name="x345">
            <para align="center">
                <text fontSize="12">${details.skuName!''}</text>
            </para>
        </rect>
        <rect col="6" align="center" row="${(details_index)+2}" colSpan="1" rowSpan="1" name="x345">
            <para align="center">
                <text fontSize="12">${details.specName!''}</text>
            </para>
        </rect>
        <rect col="7" align="center" row="${(details_index)+2}" colSpan="2" rowSpan="1" name="x345">
            <para align="center">
                <text fontSize="12">${details.remark!''}</text>
            </para>
        </rect>
        <rect col="9" align="center" row="${(details_index)+2}" colSpan="2" rowSpan="1" name="x345">
            <para align="center">
                <text fontSize="12">${details.quantity!''}</text>
            </para>
        </rect>
        <rect col="11" align="center" row="${(details_index)+2}" colSpan="1" rowSpan="1" name="x345">
            <para align="center">
                <text fontSize="12"> </text>
            </para>
        </rect>

    </#list>
        <rect col="1" align="center" row="${(count)+2}" colSpan="2" rowSpan="1" name="x345">
            <para align="center">
                <text fontSize="12">合计（小写）</text>
            </para>
        </rect>

        <rect col="3" align="center" row="${(count)+2}" colSpan="9" rowSpan="1" name="x345">
            <para align="center">
                <text fontSize="12">T=${model.sumNum!''}</text>
            </para>
        </rect>

    </table>

</para>